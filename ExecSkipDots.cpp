/*===========================================================================*\
  Copyright (c) 2015 Vlad Dobrotescu                                           
                                                                               
  Permission is hereby granted, free of charge, to any person obtaining a copy 
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights 
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
  copies of the Software, and to permit persons to whom the Software is        
  furnished to do so, subject to the following conditions:                     
                                                                               
  The above copyright notice and this permission notice shall be included in   
  all copies or substantial portions of the Software.                          
                                                                               
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE 
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN    
  THE SOFTWARE.                                                                
\*===========================================================================*/

#define  UNICODE
#define _UNICODE

#include <windows.h>
#include <wchar.h>
#include <stdio.h>

#define PROJECT_WEB_HOME L"http://bitbucket.org/vd_projects/exec_skip_dots/"
#define DEFAULT_EXE_NAME L"ExecSkipDots.exe"
#define PROGRAM_VER_TEXT L"1.0.0"

#define STRING_MAX_CHARS UNICODE_STRING_MAX_CHARS // 32767 wchars              

#define MSGBOX_TITLE_LEN 127                      // Seems like a good value   
#define MSGBOX_NUM_CHARS 2047                     // Seems like a good value   

enum ErrorNum {
  WIN32API_ERR = 1,
  CMD_TOO_LONG = 2,
  NO_PROG_FILE = 3
};

int ShowHelp(void);
int ErrorMsg(ErrorNum ErrorTag, const wchar_t *ErrorTxt=NULL, UINT ErrorVal=0);

wchar_t  MsgTitle[MSGBOX_TITLE_LEN+1]; // Default message window title         
wchar_t  TextBuff[MSGBOX_NUM_CHARS+1]; // Text shown in a MessageBox           
wchar_t  NameBuff[STRING_MAX_CHARS+1]; // Fully qualified path/name            
wchar_t  ArgsBuff[STRING_MAX_CHARS+1]; // Command line used to execute this    
wchar_t  ExecBuff[STRING_MAX_CHARS+1]; // Command line for the real program    

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
  bool  AskToRun = false;
  UINT  AskStyle = MB_OKCANCEL|MB_ICONQUESTION;
  FILE *ProgFile;
  
  wchar_t *ProgName;
  wchar_t *PathItem;
  wchar_t *FirstDot;
  wchar_t *FinalDot;
  wchar_t *ArgsLine;
  wchar_t *ExecArgs;
  size_t   QPathLen;

  STARTUPINFO         InitInfo;
  PROCESS_INFORMATION ProcInfo;

  wcscpy(MsgTitle, DEFAULT_EXE_NAME);
  
/*===========================================================================*\
  Get and process the fully qualified file name of this program                
  ---------------------------------------------------------------------------  
  There are cases when Argv[0] of the command line can't be used as it is to   
  start the original program (for example if the original program's run uses   
  the "App Path" Windows registration). The only sure way to find this name is 
  by using the fully qualified path to this file.                              
\*===========================================================================*/

  // Get the full qualified path to this file ...                              
  QPathLen = GetModuleFileName(NULL, NameBuff, STRING_MAX_CHARS);
  if (QPathLen == STRING_MAX_CHARS)
    // Highly unlikely (this program should not have been executed).           
    return ErrorMsg(WIN32API_ERR, L"GetModuleFileName()", GetLastError());
  
  // ... figure out this file's name ...                                       
  ProgName = NameBuff;
  while ((PathItem = wcspbrk(ProgName+1, L":\\/"))) ProgName = PathItem;
  ProgName++;
  wcsncpy(MsgTitle, ProgName, MSGBOX_TITLE_LEN);
  
  // ... get the positions of the first and last dot in this file's name ...   
  FirstDot = wcschr(ProgName, L'.');
  FinalDot = wcsrchr(ProgName, L'.');
  if (FirstDot == FinalDot) return ShowHelp();
  if (*(FinalDot-1) == L'.') AskToRun = true;
  
  // ... change it to the real program's name and verify it exists.            
  wcscpy(FirstDot, L".exe");
  ProgFile = _wfopen(NameBuff, L"rb");
  if (!ProgFile) return ErrorMsg(NO_PROG_FILE, ProgName);
  fclose(ProgFile);
  
/*===========================================================================*\
  Prepare the execution of the real program and run it with CreateProcess()    
  ---------------------------------------------------------------------------  
  The execution of the real program follows a  basic approach, in which only   
  the environment and the starting directory are passed to the "child" process 
  (no fancy STARTUPINFO settings). After starting the real program, this one   
  will wait for it to finish before exiting.                                   
\*===========================================================================*/
  
  // Get this program's command line and find where Argv[0] ends ...           
  ArgsLine = GetCommandLine();
  if (ArgsLine[0] == L'\"') {
    ExecArgs = wcschr(ArgsLine+1, L'\"');
    if (ExecArgs != NULL) ExecArgs++; // The "if" is for very unlikely cases.  
  } else {
    ExecArgs = wcspbrk(ArgsLine, L" \t");
  }
  if (ExecArgs == NULL) ExecArgs = ArgsLine+wcslen(ArgsLine); // No Argv[1+].  
  
  // ... build the command line for the real program ...                       
  if (wcslen(NameBuff)+wcslen(ExecArgs)+2 > STRING_MAX_CHARS)
    return ErrorMsg(CMD_TOO_LONG);
  swprintf(ExecBuff, STRING_MAX_CHARS+1, L"\"%s\"%s", NameBuff, ExecArgs);
  
  // ... ask for permission to execute this command line (if required)         
  if (AskToRun) {
    swprintf(TextBuff, MSGBOX_NUM_CHARS+1,
      L"Ready to start the real program:\t\n\n"
      L"    FILE:\t [%.*s]\t\n"
      L"    ARGS:\t [%.*s]\t\n",
      MSGBOX_TITLE_LEN, ProgName, MSGBOX_TITLE_LEN, ExecArgs);
    if (MessageBox(NULL, TextBuff, MsgTitle, AskStyle) == IDCANCEL) return 0;
  }
  
  // ... initialize the process creation data ...                              
  ZeroMemory(&InitInfo, sizeof(InitInfo));
  InitInfo.cb = sizeof(InitInfo);
  ZeroMemory(&ProcInfo, sizeof(ProcInfo));
  
  // ... and execute the real program ...                                      
  if(!CreateProcess(
      NameBuff,     // The fully qualified program name                        
      ExecBuff,     // The command line with quoted program name and arguments 
      NULL,         // Process handle not inheritable                          
      NULL,         // Thread handle not inheritable                           
      FALSE,        // Set handle inheritance to FALSE                         
      0,            // The priority flags of this process                      
      NULL,         // Use parent's environment block                          
      NULL,         // Use parent's starting directory                         
      &InitInfo,    // Pointer to the empty STARTUPINFO structure              
      &ProcInfo)    // Pointer to the empty PROCESS_INFORMATION structure      
    ) return ErrorMsg(WIN32API_ERR, L"CreateProcess()", GetLastError());

  // ... wait until the real program process exits ...                         
  WaitForSingleObject(ProcInfo.hProcess, INFINITE);

  // ... and clean up (close process and thread handles) before exiting.       
  CloseHandle(ProcInfo.hProcess);
  CloseHandle(ProcInfo.hThread);
  return 0;
}

int ErrorMsg(ErrorNum ErrorTag, const wchar_t *ErrorTxt, UINT ErrorVal) {
  switch(ErrorTag) {
    case WIN32API_ERR:
      swprintf(TextBuff, MSGBOX_NUM_CHARS,
        L"WIN32 API ERROR:\t\n\n"
        L"    %s: Error %d\t\n",
        ErrorTxt, ErrorVal);
      break;
    case CMD_TOO_LONG:
      swprintf(TextBuff, MSGBOX_NUM_CHARS,
        L"PROGRAM LAUNCHING ERROR:\t\n\n"
        L"    Resulting command line exceeds %d chars\t",
        STRING_MAX_CHARS);
      break;
    case NO_PROG_FILE:
      swprintf(TextBuff, MSGBOX_NUM_CHARS,
        L"PROGRAM LAUNCHING ERROR:\t\n\n"
        L"    Missing the real program:\t\n\n"
        L"        \"%.*s\"\t\n",
        MSGBOX_TITLE_LEN, ErrorTxt);
      break;
  }
  MessageBox(NULL, TextBuff, MsgTitle, MB_OK|MB_ICONERROR);
  return ErrorTag;
}

int ShowHelp(void) {
  MessageBox(NULL, 
    L"This program analyzes the name used to execute it. If it finds more "
     "than one dot in its name, it will generate a new \t\n"
    L"program name by replacing everything after the first dot with \"exe\". "
     "It will then run the program with this name (if \t\n"
    L"available in its own folder), wait for it to finish, and exit. If it is "
     "properly renamed and placed in the launch chain of \t\n"
    L"a PortableApps.com program, " DEFAULT_EXE_NAME L" will provide a unique "
     "name to be monitored by the Launcher, thus \t\n"
    L"allowing multiple versions of the same program (portable or not) to be "
     "run concurrently (but ONLY if the Launcher's \t\n"
    L"name confusion is the last remaining issue stopping this).\t\n"
    L"\n"
    L"If the program's name doesn't have two or more dots, it will just "
     "display this help screen when it is executed.\t\n"
    L"\n"
    L"HOW TO USE IT\n"
    L"\n"
    L"For illustration purpose, the program to be executed is named "
     "PortProg.exe, part of a PortProgPortable subfolder \t\n"
    L"of a PortableApps.com root. \t\n"
    L"    1.  Copy " DEFAULT_EXE_NAME L" to the folder where PortProg.exe "
     "resides; \t\n"
    L"    2.  Rename " DEFAULT_EXE_NAME L" to \"PortProg.SomeLabel.exe\" "
     "(\"SomeLabel\" can be anything Windows allows); \t\n"
    L"    3.  Locate the file PortProgPortable.ini in the "
     "\"App\\AppInfo\\Launcher\" subfolder and open it for editing; \t\n"
    L"    4.  Find the ProgramExecutable=... line in the [Launch] section of "
     "this file; \t\n"
    L"    5.  Replace PortProg.exe with PortProg.SomeLabel.exe at the end of "
     "this line and save the file. \t\n"
    L"\n"
    L"If it makes sense for your use case, you can use Resource Hacker (or a "
     "similar program) to copy the icon group(s) \t\n"
    L"from PortProg.exe to PortProg.SomeLabel.exe and/or change the "
     "\"Description\" field of its \"Version Info\" resource \t\n"
    L"to something relevant. \t\n"
    L"\n"
    L"For debug purposes, if the last dot in the program's name is doubled "
     "(as in \"PortProg.SomeLabel..exe\") a message \t\n"
    L"box (showing the command line to be used) will pop up before "
     "PortProg.exe is started, allowing its execution to be \t\n"
    L"canceled. \t\n"
    L"\n"
    L"USAGE WARNING\n"
    L"\n"
    L"The PortProgPortable.ini file contains important settings related to "
     "the Registry and the File System interactions of \t\n"
    L"the portable version of ProgPort.exe with your system; you should "
     "address any other issues that could affect the \t\n"
    L"simultaneous execution of multiple versions of PortProg.exe before "
     "trying to do it. \t\n"
    L"\n"
    L"USEFUL LINKS\n"
    L"\n"
    L"Project's Home:\t" PROJECT_WEB_HOME L"\t\n"
    L"Resource Hacker:\thttp://www.angusj.com/resourcehacker/\t\n"
    L"\n"
    L"        HINT: Pressing Ctrl-C when this window has focus will copy its "
     "entire contents to the clipboard, as text.\t", 
    DEFAULT_EXE_NAME L" - v" PROGRAM_VER_TEXT,
    MB_OK|MB_ICONINFORMATION);
  return 0;
}

