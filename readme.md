# ExecSkipDots.exe

This small Windows program analyzes the name used to execute it. If it finds 
more than one dot in its name, it will generate a new program name by 
replacing everything after the first dot with "**exe**". It will then run the 
program with this name (if available in its own folder), wait for it to 
finish, and exit. If it is properly renamed and placed in the launch chain of 
a **PortableApps.com** program, **ExecSkipDots.exe** will provide a unique 
name to be monitored by the Launcher, thus allowing multiple versions of the 
same program (portable or not) to be run concurrently (but **ONLY** if the 
**Launcher**'s name confusion is the last remaining issue stopping this).

-------------------------------------------------------------------------------

For a more detailed description, see the project's [Wiki][] page.

[Wiki]: https://bitbucket.org/vd_projects/exec_skip_dots/wiki
